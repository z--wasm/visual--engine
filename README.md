# wasm-test-rust

Testing Rust on the web:
https://mikko-timofeev.gitlab.io/wasm-rustTest

## Containered development

```
  bash podman/build  # prepare environment
  bash podman/start  # start service (hosted at localhost:8080)

  # start development

  bash podman/app_compile  # compile WASM files from source
  bash podman/app_console  # (optionally) explore environment

  # finish development

  bash podman/stop  # stop service
```
