FROM rust:1.52.1-alpine
RUN apk add --no-cache bash curl build-base npm
RUN npm install -g http-server uglify-js
RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
WORKDIR /app

ARG LOCAL_USER=root
ARG LOCAL_GROUP=root
ARG LOCAL_UID=0
ARG LOCAL_GID=0

RUN addgroup --gid $LOCAL_GID $LOCAL_GROUP || true \
    && adduser -u $LOCAL_UID -G $LOCAL_GROUP $LOCAL_USER --disabled-password || true \
    && chown $LOCAL_USER:$LOCAL_GROUP /app

USER $LOCAL_USER
RUN echo "Rust build environment for $(whoami)"
