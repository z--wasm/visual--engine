#!/bin/sh

wasm-pack build \
  --release \
  --no-typescript \
  --target web \
  --out-name core \
  --out-dir tmp \
  || exit 1

echo '-----'
ls tmp # view compiled
echo '-----'

cp tmp/core_bg.wasm \
  public/scripts \
  || exit 1

uglifyjs \
  "tmp/core.js" \
  --compress \
  --mangle \
  -o "public/scripts/core.js" \
  || exit 1
