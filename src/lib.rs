mod utils;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet(who: &str) -> String {
    // format!("Hello, {you}!\nToday is {wday}", you = who, wday = "a good day").into()
    format!(include_str!("presets/hello.xml"), you = who, wday = "a good day").into()
    // alert("Hello, friend!!!");
}
